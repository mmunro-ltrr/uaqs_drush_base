# Base PHP image with Composer and Drush

This is derived from the official Debian image, not from the official
Drush image, nor from its base, or the current Composer official image.
The PHP version is fixed at whatever is current in Debian, included as
deb binary packages, rather than being built from source.
